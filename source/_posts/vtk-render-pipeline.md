---
title: VTK 可视化管线和渲染引擎
date: 2023-06-28 21:39:03
tags: vtk
description: 说明VTK运行逻辑及介绍如何自定义Filter
---

[TOC]

# 综述

![1687965275959](1687965275959.png)

vtk中程序中通常包含两部分, **Rendering Engine**渲染引擎 和 **Visualization Pipeline**可视化管线，可视化管线用于获取和创建数据、处理数据，最终把处理后得到的可可供渲染使用的数据，将数据传递给Writer写入文件或者RenderEngine进行展示。在可视化管线执行的过程中，会包含很多类型的filter，其主要作用就是将输入的数据进行处理后再进行输出，即对输入数据的加工处理，也就是我们最熟悉和接触最多的算法处理部分。RenderEngine包含片元渲染和体绘制两种类型，其主要作用是对可视化管线处理后的数据进行展示或者显示，这里比较关注的是显示方式，入灯光、相机位置、视口大小、物体材质等等。

# 可视化管线

## 可视化管线连接方式

前面说到，可视化管线中核心部分为各种类型的filter，这些filter对输入数据进行处理再输出出去，我们的核心算法都放在filter当中，软件执行过程中可以组合不同的filter以实现我们对数据进行各种各样的处理，再VTK中可视化管线支持三种连接方式：

- 单输入-单输出
- 单输入-多输出
- 多输入-多输出

![img](20170111220338447.png)

## 可视化管线如何执行和Update

![img](20170111221543198.png)



![img](20170111222652959.png)

使用SetInputConnection()和GetOutPort()连接Filter之后，数据并没有真正的在管线中流动起来，这个时候需要调用Update方法，才会让Filter内部算法执行起来，从而推动数据在pipeline中流动起来，比如：

```c++
auto reader = vtkSmartPointer<vtkJPEGReader>::New();
reader->SetFileName("Test.jpg");
reader->Update();
```

但是通常，我们不显示调用Update()方法，因为在Rendering Engine后，调用Render()方法时，会触发Update()方法，然后pipeline中会逐级向上传播调用(可能是递归调用？)，然后管线中会逐级的返回执行结果。

## 自定义vtkFilter

vtk中每个Filter可以分为两部分：

- 算法部分，继承自vtkAlgorithm, 负责处理输入的数据和信息
- 执行对象，继承自vtkExecutive，负责通知算法对象何时运行以及传递需要吹的数据和信息

Filter类继承自vtkAlgorithm机器子类，实例化时内部会生成一个默认的vtkExecutive对象，用于管理执行过程。数据和信息通过端口在Filter中进行传递，根据数据流的方向，分为输入端口和输出端口。Filter之间通过端口（Port）建立连接（Connect）。例如一个标准的连接如下：

```c++
FilterTemp->SetInputConnection(FilterTemp0->GetOutPort());
```

### 管线执行模型

请求是VTK执行管线的一个基本操作，一个管线执行模型右多个请求共同完成。管线中处理一个Filter的请求 时，通常会先传递至为其提供数据输入的上流Filter，待上流的Filter处理完请求再在当前Filter中进行处理。执行对象在处理请求时，通常需要算法对象的协助来完成。执行对象会将请求发送至算法对象，并由vtkAlgorithm::ProcessRequest()进行处理。所有Filter类中都会实现该函数，并处理相关请求。管线执行过程如下图所示。

![这里写图片描述](SouthEast.png)

### 自定义Filter基本步骤

实现Filter类时需要根据需求选择合适的基类，vtkAlgorithm是所有Filter类的共同基类，通常很少直接使用其 作为基类，而是更多的是采用其子类。比如，图形类Filter通常使用vtkPolyDataAlgorithm或者vtkUnstructedGridAlgorithm作为基类。 实现一个Filter，需要以下三个重要步骤。

1. 定义管线接口
2. 定义用户接口
3. 实现管线请求

# 渲染引擎

vtk渲染引擎由VTK中的类组成，这些类负责获取可视化管道的结果并将其显示到窗口中或写入到文件中,具体说明见参考[3]。

- vtkProp： vtkActor和 vtkVolume的基类
- vtkAbstractMapper
- vtkProperty&vtkVolumeProperty
- vtkCamera
- vtkLight
- vtkRenderer
- vtkRenderWindow
- vtkRenderWindowInteractor
- vtkTransform
- vtkLookupTable\&vtkColorTransferFunction&vtkPiecewiseFunction

# 参考

[1] [(210条消息) VTK-对Filter的理解_vtk filter_我的辉的博客-CSDN博客](https://blog.csdn.net/huihui__huihui/article/details/114239818)

[2] [VTK 可视化管道的连接与执行 - 一杯清酒邀明月 - 博客园 (cnblogs.com)](https://www.cnblogs.com/ybqjymy/p/14241006.html)

[3] [(210条消息) VTK渲染引擎_vtk 渲染_SylarXillee的博客-CSDN博客](https://blog.csdn.net/xillee388366/article/details/127937103)

[4] [(213条消息) 转载-VTK Filter 总结_黑山老妖的笔记本的博客-CSDN博客](https://blog.csdn.net/liushao1031177/article/details/117412843?spm=1001.2101.3001.6650.3&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-3-117412843-blog-75905519.235^v38^pc_relevant_sort&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-3-117412843-blog-75905519.235^v38^pc_relevant_sort)
