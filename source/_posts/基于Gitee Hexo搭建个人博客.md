---
title: 基于Gitee Hexo 搭建个人博客
date: 2021-03-19 16:27:01
tag: 博客搭建
categories: 博客搭建
comment: true
description: 根据本文章尝试一步一步搭建自己的博客网站
---


# 基于Gitee + Hexo搭建个人博客

## 准备

> nodejs下载, 官网: https://nodejs.org/zh-cn/
>
> Git 下载安装: 官网: https://git-scm.com/

查看node.js版本(我的是12.13.0):

```bash
node -v
```



## Hexo博客模板

> Hexo 官网: https://hexo.io/zh-cn/

Linux系统下按照如下顺序:

1.  安装Hexo: 

   ```bash
   npm install hexo-cli -g
   ```

   

2. 安装博客部署插件:

   ```bash
   npm install hexo-deployer-git --save
   ```

3. 新建博客文件夹: 

   ```bash
   mkdir MyWeb
   ```

   

4. 跳转到文件夹中: cd MyWeb

   ```bash
   cd MyWeb
   ```

   

5. 初始化hexo模板: 

   ```bash
   hexo init
   ```

6. 运行hexo

   ```bash
   hexo clean # 清空上次生成的网站文件
   hexo g # 生成新的网页文件
   hexo s # 启动网站, 默认地址: localhost:4000. Ctrl + C 中断服务器
   ```

6. 设置主题

   从[官网](https://hexo.io/themes/) 查找自己喜欢的主题, 找到github库,然后通过下载zip文件将主题文件下载下来,然后解压文件,将主题内容复制到刚刚新建网站的themes文件夹下:

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211142854559.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211133706188.png)



7. 修改MyWeb主目录下_config.yml中的theme改为新的主题名称,如按照上面图片内容修改后配置如下:

   ```yml
   theme: sakura
   ```

8. 重新测试运行:

   ```bash
   hexo clean
   hexo g
   hexo s
   ```

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211142624967.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2N1bmd1ZGFmYQ==,size_16,color_FFFFFF,t_70)

## 码云配置

1. 在码云中新建仓库:

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211140208164.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2N1bmd1ZGFmYQ==,size_16,color_FFFFFF,t_70)

2. 获取仓库地址:

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211140438382.png)

3. 修改MyWeb下的_config.yml中下列内容:

   ```yml
   deploy:
   	type: git 
   	repo: https://gitee.com/****/***/.git 
   	branch: master 
   ```

4. 设置_config.yml中的根路径(repository_name 为新建的代码仓库名称):

   ```yml
   url:  http://****.gitee.io/repository_name
   root: /repository_name
   ```

5. 部署博客:

   ```bash
   npm install hexo-deployer-git --save
   hexo g --g # 一键部署后,代码将会上传到_config.yml中设置的git远程仓库地址
   ```

6. 开启gitee的Gitee Page功能:

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200211144338149.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2N1bmd1ZGFmYQ==,size_16,color_FFFFFF,t_70)

7. 根据后续提示访问博客地址,确认配置无误.

## 博客使用

1. 在..\MyWeb\source\\_post文件夹下添加新的博客文件,文件为markdown格式,后缀为.md. 

2. 博客文件需要有如下格式的文件头:

   ```
   ---
   title: file_name
   tag: 标签名
   categories: 分类
   comment: 是否允许评论(true or false)
   description: 描述
   ---
   文档正文编写，请参照markdown语法。
   ```

3. 执行hexo命令将文章发布到远程仓库:

   ```
   hexo g --d
   ```

   

4. 在Gitee Page服务中选择更新服务
5. 刷新博客网址,确认文档是否已更新

## 主要命令:

- Hexo
  - Hexo clean # 清理旧的网站内容
  - hexo g # 生成新的网页文件
  - hexo s # 在本机运行hexo server
  - hexo g —d # 部署网站到远程仓库上
  - hexo new [layout] title # 创建新的文章

## 参考文章

本文基本上是与参考文章一样的,只对部分内容做了小幅修改,作为第一篇博客示例,也防止后续找不到记录:

1. [基于Gitee + Hexo搭建个人博客](https://blog.csdn.net/cungudafa/article/details/104260494)
2. [Gitee官方说明](https://gitee.com/help/articles/4136#article-header0)

