---
layout: post
title: Hexo中插入本地图片的方法
date: 2022-04-04 16:00:07
tags: [博客搭建]
categores: [博客搭建 hexo]
description: 介绍如何在hexo的md文件中添加本地图片
---

# Hexo中插入本地图片的方法

## 基本设置

Hexo的markdown文章中通常会使用相对路径来设置图片路径，这样方便将代码发布到服务器上面。在插入图片之前，需要修改博客根目录下的**_config.yaml**文件：

```yaml
post_assset_folder: true
```

添加上面的设置后，通过hexo new创建文章的时候，hexo会帮我们自动创建与文章同名的图片资源文件夹。

> 由于项目会生成新的文件目录，同时会解析Markdown中的图片路径，会导致一个问题。
如在一个文件目录下，博客名为`1.md`，相应的存在一个`1`文件夹存放图片`image.jpg`。
在Typora编辑器中，普通的md文件使用`![](1/image.jpg)`能在编辑器中正常显示图片。
在hexo中，按理说应该是使用`![](image.jpg)`，但网页中却无法正常显示。
此时应该使用这样的方式来引入图片：
{% asset_img image.jpg 这是一张图片 %}

这样的话，虽然可以正常的引用图片，但是使用上实在是不方便。为了解决该问题，则有了下面的工具 hexo-renderer-marked。

## 图片插件

安装：

```bash
npm install hexo-renderer-marked
```

配置 _config.yaml

```yaml
post_asset_folder: true
marked:
	prependRoot: true
	postAsset: true
```

配置好之后，文章上就可以按照下面方式插入图片了

```markdown
![图像说明](image.jpg)
```

## Hexo 与Typora结合

Typora可以设置将图片自动保存到指定路径，当从网页复制图像或者复制截图时，typora会帮助用户将图片复制到该路径下，设置如下：

![image-20220404163947818](image-20220404163947818.png)

设置好后，需要注意一点，markdown文章中自动填充的路径为文章名称文件夹下的图片路径，再将文章同步到服务器的时候，需要将路径中文章资源文件夹名称去除，只保留资源文件夹以内的文件路径才行。最终发布到服务器，我们就能够找到对应的图像资源了。
