---
title:  Angular 响应resize事件
date: 2021-03-23 16:27:01
tag: Angular
categories: Angular
comment: true
description: Angular 响应resize事件
---
# Angular 响应resize事件

在利用angular进行开发的时候，通过**数据绑定+angular生命周期钩子**我们可以实现大部分的事件响应相关功能开发，但是在处理组件resize事件的时候，发现原生的angular提供的工具都失效了，最后在网上找了挺长时间，发现有以下方法可以解决上述问题。

## CSS Element Queries

css-element-queries组件是一个用来监听元素尺寸变化的npm组件，在该组件中定义了一个ResizeSensor类用来处理resize事件，在创建的时候只需要将dom元素对象和回调对象即可，具体的介绍可以参考：https://github.com/marcj/css-element-queries

实际使用代码如下：

```typescript
@Component({
  selector: 'app-my-component',
  styleUrls: ['./my.component.less'],
  templateUrl: './my.component.html',
})
export class MyComponent implements OnInit, OnDestroy  {

  // 定义一个resizeSensor对象
  private resizeSensor: ResizeSensor;

  constructor(private el: ElementRef) { }
    
  ngOnInit(): void {
    // only initialize resize once
    if (this.resizeSensor) {
      this.resizeSensor = new ResizeSensor(this.element.nativeElement, this.onResized);
    }
  }

  ngOnDestroy(): void {
    // detach bind.
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  public onResize(e: {width: number, height: number}) { 
  	console.log(`Height: ${e.height}, Width: ${e.width}`);
  }
}

```



## 通过安装angular-resize-event第三方库实现

### 使用介绍

前边的方法虽然可以实现resize事件的监听，但是写着总是觉得有些不舒服，毕竟不像是angular定义的指令一样进行事件绑定，不过还好，在搜索引擎帮助下我找到了另一个lib：**angular-resize-event**, 通过引用这个库，我们可以对component中的dom element像绑定其他事件一样将回调函数绑定到resize上，代码如下：

1. 通过npm安装依赖：

   ```bash
   $ npm install angular-resize-event
   ```

2. 将 模块添加到AppModule中或者自己定义的Module中：

   ```typescript
   import { BrowserModule } from '@angular/platform-browser';
   import { NgModule } from '@angular/core';
   import { AppComponent } from './app.component';
   
   // Import the library module
   import { AngularResizedEventModule } from 'angular-resize-event';
   
   @NgModule({
     declarations: [
       AppComponent
     ],
     imports: [
       BrowserModule,
   
       // Specify AngularResizedEventModule library as an import
       AngularResizedEventModule
     ],
     providers: [],
     bootstrap: [ AppComponent ]
   })
   export class AppModule { }
   ```

3. 在前端代码中添加resized事件绑定：

   ```html
   <div class="container"
     (resized) = "onResized($event)" >
   	<H1>测试resize事件</H1>
   </div>
   ```

4. 后端代码中添加回调函数：

   ```typescript
   import { Component } from '@angular/core';
   
   // Import the resized event model
   import { ResizedEvent } from 'angular-resize-event';
   
   @Component({...})
   class MyComponent {
     width: number;
     height: number;
   
     onResized(event: ResizedEvent) {
       this.width = event.newWidth;
       this.height = event.newHeight;
     }
   }
   ```

### 源码解析

​	上边的代码能够看出，我们能够很轻松的使用angular-resize-event实现resize事件的绑定，然后我们再看下组件的实际运行源码，这样我们以后也就不需要安装第三方依赖自己实现类似的功能了：

```typescript
import { Directive, ElementRef, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { ResizeSensor } from 'css-element-queries';
import { ResizedEvent } from './resized-event';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[resized]'
})
export class ResizedDirective implements OnInit, OnDestroy {

  @Output()
  readonly resized = new EventEmitter<ResizedEvent>();

  private oldWidth: number;
  private oldHeight: number;

  private resizeSensor: ResizeSensor;

  constructor(private readonly element: ElementRef) {
  }

  ngOnInit(): void {
    // only initialize resize watching if sensor is availablei
    if (ResizeSensor) {
      this.resizeSensor = new ResizeSensor(this.element.nativeElement, () => this.onResized());
    }
  }

  ngOnDestroy(): void {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  private onResized(): void {
    const newWidth = this.element.nativeElement.clientWidth;
    const newHeight = this.element.nativeElement.clientHeight;

    if (newWidth === this.oldWidth && newHeight === this.oldHeight) {
      return;
    }

    const event = new ResizedEvent(
      this.element,
      newWidth,
      newHeight,
      this.oldWidth,
      this.oldHeight
    );

    this.oldWidth = this.element.nativeElement.clientWidth;
    this.oldHeight = this.element.nativeElement.clientHeight;

    this.resized.emit(event);
  }

}
```

这个resize模块功能实现也比较简单，

1. 首先作者自定义了一个ResizeDirective，在指令的初始化过程中通过ResizeSensor(有没有很熟悉？:smirk: 其实就是CSS Element Queries里的那个Sensor)订阅了指定dom元素的resize事件，这里边onResize函数是自己去查询了dom元素的宽度和高度，有些画蛇添足的感觉，不过也可能是大佬另有深意吧。

2. 然后再通过EventEmitter的输出变量，将尺寸信息输出给了订阅者。

3. 最后在ngOnDestroy方法中对resizeSensor进行了解绑。

   

   到此，整个resize绑定指令都已经实现了，实现比较简单，但是也比较有参考意义，放在这里给大家分享一下 :smile:。

## 参考：

[angularjs中如何实现页面自适应？-js教程-PHP中文网](https://www.php.cn/js-tutorial-402790.html)

[angular-resize-event - npm (npmjs.com)](https://www.npmjs.com/package/angular-resize-event)

