---
title: VS2022下搭建gRPC环境
date: 2022-07-11 16:15:39
tags: gRPC
author: seven
description: 在windows环境下，利用vcpkg搭建vs2022 grpc C++开发环境
---

# gRPC介绍

![img](v2-13d685915ee28ac36b80b110d1deecca_720w.jpg)

>所谓RPC(remote procedure call 远程过程调用)框架实际是提供了一套机制，使得应用程序之间可以进行通信，而且也遵从server/client模型。使用的时候客户端调用server端提供的接口就像是调用本地的函数一样。

## gRPC vs. Restful

gRPC和restful API都提供了一套通信机制，用于server/client模型通信，而且它们都使用http作为底层的传输协议(严格地说, gRPC使用的http2.0，而restful api则不一定)。不过gRPC还是有些特有的优势，如下：

- gRPC可以通过protobuf来定义接口，从而可以有更加严格的接口约束条件。关于protobuf可以参见笔者之前的小文[Google Protobuf简明教程](https://www.jianshu.com/p/b723053a86a6)
- 另外，通过protobuf可以将数据序列化为二进制编码，这会大幅减少需要传输的数据量，从而大幅提高性能。
- gRPC可以方便地支持流式通信(理论上通过http2.0就可以使用streaming模式, 但是通常web服务的restful api似乎很少这么用，通常的流式数据应用如视频流，一般都会使用专门的协议如HLS，RTMP等，这些就不是我们通常web服务了，而是有专门的服务器应用。）

## 使用场景

- 需要对接口进行严格约束的情况，比如我们提供了一个公共的服务，很多人，甚至公司外部的人也可以访问这个服务，这时对于接口我们希望有更加严格的约束，我们不希望客户端给我们传递任意的数据，尤其是考虑到安全性的因素，我们通常需要对接口进行更加严格的约束。这时gRPC就可以通过protobuf来提供严格的接口约束。
- 对于性能有更高的要求时。有时我们的服务需要传递大量的数据，而又希望不影响我们的性能，这个时候也可以考虑gRPC服务，因为通过protobuf我们可以将数据压缩编码转化为二进制格式，通常传递的数据量要小得多，而且通过http2我们可以实现异步的请求，从而大大提高了通信效率。

但是，通常我们不会去单独使用gRPC，而是将gRPC作为一个部件进行使用，这是因为在生产环境，我们面对大并发的情况下，需要使用分布式系统来去处理，而gRPC并没有提供分布式系统相关的一些必要组件。而且，真正的线上服务还需要提供包括负载均衡，限流熔断，监控报警，服务注册和发现等等必要的组件。

# vcpkg配置

## vcpkg安装

```bash
git clone https://github.com/Microsoft/vcpkg.git
cd vcpkg
bootstrap-vcpkg.bat
```

初始化后，可以将vcpkg文件夹加入环境变量中，方便后面使用。

## gRPC依赖库安装

在windows中启动控制台并执行命令：

```bash
vcpkg install grpc:x64-windows
```

安装protobuf：

```bash
vcpkg install protobuf protobuf:x64-windows
```

如果需要zlib支持，还需要安装zlib功能：

```bash
vcpkg install protobuf[zlib] protobuf[zlib]:x64-windows
```

**Note**：理想情况下，如果您已经安装了 Visual Studio，这应该不会出现任何错误。

安装后，您可以在文件夹 <vcpkg_installed_path>\packages 下的 vcpkg 目录中找到已安装的软件包。 为了使软件包在系统范围内可用，您需要使用命令：

```bash
vcpkg integrate install
```

如果只想工程可见，需要执行下面的命令：

```bash
vcpkg integrate project
```

## VS2022 环境配置

## nuget配置

在VS工具栏找到：工具->NuGet包管理器->程序包源，点击绿色加号添加新的程序包源，源设置为vcpkg路径下的scripts\buildsystems：

![image-20220711172728238](image-20220711172728238.png)

在NuGet解决方案中，将vcpkg包加载到需要的project当中。

![image-20220711174258650](image-20220711174258650.png)

## 添加静态链接库

在工程中，点击属性，找到连接器->输入->附加依赖项，将gRPC相关的lib文件添加到输入框中：

```
grpc++_unsecure.lib
grpcpp_channelz.lib
grpc_csharp_ext.lib
grpc++_reflection.lib
grpc++_error_details.lib
grpc_plugin_support.lib
grpc++_alts.lib
grpc.lib
grpc++.lib
grpc_unsecure.lib
libprotobufd.lib
libprotobuf-lited.lib
libprotocd.lib
WS2_32.Lib
```

# 测试程序

## 创建proto文件

创建helloWorld.proto文件，定义service以及消息类型：

```protobuf
syntax = "proto3";

option java_package = "io.grpc.examples";

package helloworld;

// The greeter service definition.
service Greeter {
  // Sends a greeting
  rpc SayHello (HelloRequest) returns (HelloReply) {}
}

// The request message containing the user's name.
message HelloRequest {
  string name = 1;
}

// The response message containing the greetings
message HelloReply {
  string message = 1;
}
```

## 编译proto文件并生成c++文件

通过vcpkg安装好protobuf和gRPC后，可以在..vcpkg\installed\x64-windows\tools\protobuf路径下找到protoc.exe文件用于生成cpp文件，..vcpkg\installed\x64-windows\tools\grpc下可以找到grpc_cpp_plugin.exe文件，用于生成service的代码文件。

则通过下面的命令可以生成对应的代码文件：

```bash
.\tools\protoc.exe --grpc_out=./grpc/ --plugin=protoc-gen-grpc=.\tools\grpc_cpp_plugin.exe .\helloWorld.proto
.\tools\protoc.exe --cpp_out=./app/ ./helloWorld.proto
```

上面命令后，会在./grpc和./app/文件夹下生成四个文件,将这四个文件加入到server端代码和client端工程当中：

- helloWorld.pb.cc
- helloWorld.pb.h
- helloWorld.grpc.pb.cc
- helloWorld.grpc.pb.h

在server端，代码如下：

```c++
#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "helloWorld.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using helloworld::HelloRequest;
using helloworld::HelloReply;
using helloworld::Greeter;

// Logic and data behind the server's behavior.
class GreeterServiceImpl final : public Greeter::Service {
    Status SayHello(ServerContext* context, const HelloRequest* request, HelloReply* reply) override {
	    const std::string prefix("Hello ");
        const auto name = request->name();
        printf("SayHello Is Called.\n");
        reply->set_message(prefix + name);
        return Status::OK;
    }
};

void RunServer() {
	const std::string serverAddress("0.0.0.0:50051");
    GreeterServiceImpl service;

    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(serverAddress, grpc::InsecureServerCredentials());
    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    builder.RegisterService(&service);
    // Finally assemble the server.
	const std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << serverAddress << std::endl;

    // Wait for the server to shutdown. Note that some other thread must be
    // responsible for shutting down the server for this call to ever return.
    server->Wait();
}

int main(int argc, char* argv[])
{
	printf("Hello gRPC\n");

    RunServer();

    printf("Server Exit\n");
}
```

Client端代码如下：

```c++
#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "helloWorld.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using helloworld::HelloRequest;
using helloworld::HelloReply;
using helloworld::Greeter;

class GreeterClient {
public:
	explicit GreeterClient(const std::shared_ptr<Channel> channel)
        : stub_(Greeter::NewStub(channel)) {}

    // Assembles the client's payload, sends it and presents the response back
    // from the server.
    std::string SayHello(const std::string& user) {
        // Data we are sending to the server.
        HelloRequest request;
        request.set_name(user);

        // Container for the data we expect from the server.
        HelloReply reply;

        // Context for the client. It could be used to convey extra information to
        // the server and/or tweak certain RPC behaviors.
        ClientContext context;

        // The actual RPC.
        Status status = stub_->SayHello(&context, request, &reply);

        // Act upon its status.
        if (status.ok()) {
            return reply.message();
        }
        else {
            std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
            return "RPC failed";
        }
    }

private:
    std::unique_ptr<Greeter::Stub> stub_;
};

int main(int argc, char** argv) {
    // Instantiate the client. It requires a channel, out of which the actual RPCs
    // are created. This channel models a connection to an endpoint (in this case,
    // localhost at port 50051). We indicate that the channel isn't authenticated
    // (use of InsecureChannelCredentials()).
    GreeterClient greeter(grpc::CreateChannel(
        "127.0.0.1:50051", grpc::InsecureChannelCredentials()));
    std::string user("world");
    std::string reply = greeter.SayHello(user);
    std::cout << "Greeter received: " << reply << std::endl;

    return 0;
}
```

## 测试

- 运行server端可执行文件
- 运行Client端可执行文件

![image-20220711181458626](image-20220711181458626.png)

# 参考文档

[gRPC详解 - 简书 (jianshu.com)](https://www.jianshu.com/p/9c947d98e192)

[Vcpkg——C++包管理工具 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/87391067)
