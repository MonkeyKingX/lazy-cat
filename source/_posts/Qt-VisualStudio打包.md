---
title: Qt VisualStudio打包
date: 2022-06-15 09:40:43
tags: Qt, C++
author: seven
description: 介绍Qt & Visual Studio如何进行打包
---

# Qt Visual Studio打包

![img](1998400-20200409180223417-1293700826.png)

- 根据自己的编译环境，选择Qt命令行窗口，如我的VS版本为2022，Qt编译器使用的是MSVC 2017,这里选择 Qt 5.12.4 (MSVC 2017 64-bit), 点击并打开
- 通过命令行cd重定向到VS编译release版本输出目录
- 运行命令行： windeployqt  可执行文件

**windeployqt命令行参数**

```bash
Usage: windeployqt [options] [files]
Qt Deploy Tool 5.14.1

The simplest way to use windeployqt is to add the bin directory of your Qt
installation (e.g. <QT_DIR\bin>) to the PATH variable and then run:
  windeployqt <path-to-app-binary>
If ICU, ANGLE, etc. are not in the bin directory, they need to be in the PATH
variable. If your application uses Qt Quick, run:
  windeployqt --qmldir <path-to-app-qml-files> <path-to-app-binary>

Options:
  -?, -h, --help            Displays help on commandline options.
  --help-all                Displays help including Qt specific options.
  -v, --version             Displays version information.
  --dir <directory>         Use directory instead of binary directory.
  --libdir <path>           Copy libraries to path.
  --plugindir <path>        Copy plugins to path.
  --debug                   Assume debug binaries.
  --release                 Assume release binaries.
  --pdb                     Deploy .pdb files (MSVC).
  --force                   Force updating files.
  --dry-run                 Simulation mode. Behave normally, but do not
                            copy/update any files.
  --no-patchqt              Do not patch the Qt5Core library.
  --no-plugins              Skip plugin deployment.
  --no-libraries            Skip library deployment.
  --qmldir <directory>      Scan for QML-imports starting from directory.
  --qmlimport <directory>   Add the given path to the QML module search
                            locations.
  --no-quick-import         Skip deployment of Qt Quick imports.
  --no-translations         Skip deployment of translations.
  --no-system-d3d-compiler  Skip deployment of the system D3D compiler.
  --compiler-runtime        Deploy compiler runtime (Desktop only).
  --no-virtualkeyboard      Disable deployment of the Virtual Keyboard.
  --no-compiler-runtime     Do not deploy compiler runtime (Desktop only).
  --webkit2                 Deployment of WebKit2 (web process).
  --no-webkit2              Skip deployment of WebKit2.
  --json                    Print to stdout in JSON format.
  --angle                   Force deployment of ANGLE.
  --no-angle                Disable deployment of ANGLE.
  --no-opengl-sw            Do not deploy the software rasterizer library.
  --list <option>           Print only the names of the files copied.
                            Available options:
                             source:   absolute path of the source files
                             target:   absolute path of the target files
                             relative: paths of the target files, relative
                                       to the target directory
                             mapping:  outputs the source and the relative
                                       target, suitable for use within an
                                       Appx mapping file
  --verbose <level>         Verbose level (0-2).

Qt libraries can be added by passing their name (-xml) or removed by passing
the name prepended by --no- (--no-xml). Available libraries:
bluetooth concurrent core declarative designer designercomponents enginio
gamepad gui qthelp multimedia multimediawidgets multimediaquick network nfc
opengl positioning printsupport qml qmltooling quick quickparticles quickwidgets
script scripttools sensors serialport sql svg test webkit webkitwidgets
websockets widgets winextras xml xmlpatterns webenginecore webengine
webenginewidgets 3dcore 3drenderer 3dquick 3dquickrenderer 3dinput 3danimation
3dextras geoservices webchannel texttospeech serialbus webview

Arguments:
  [files]                   Binaries or directory containing the binary.
```

# 参考文档

[Qt Deploy发布程序 - lccsuse - 博客园 (cnblogs.com)](https://www.cnblogs.com/leocc325/p/12668610.html)

[Qt for Windows - Deployment | Qt 5.15](https://doc.qt.io/qt-5/windows-deployment.html#the-windows-deployment-tool)
