---
title: Hexo 几款插件推荐（持续更新）
date: 2021-03-20 16:27:01
tag: 博客搭建
categories: 博客搭建
comment: true
description: Hexo 几款插件推荐
---

# Hexo 几款插件推荐（持续更新）

1. 一键部署插件：hexo-deployer-git
2. hexo后台管理插件：[hexo-admin](https://github.com/jaredly/hexo-admin)
3. 日历插件： [theme-next-calendar](https://github.com/icecory/theme-next-calendar)
4. 表情工具{% emoji tada %} ： [hexo-filter-emoji](https://github.com/theme-next/hexo-filter-emoji)