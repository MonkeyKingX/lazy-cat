---
layout: post
title: C++调用Python进行混合编程
date: 2022-06-12 18:31:12
tags: C++
categories: C++
author: seven
description: C++调用Python进行混合编程
---

# Visual Studio project配置

![image-20220612183505279](image-20220612183505279.png)

- 在属性管理器中，配置VC++目录中的包含目录，路径为Python安装目录下的include文件夹
- 在属性管理器中，配置库目录，路径为Python安装目录下的libs文件夹
- ![image-20220612183733566](image-20220612183733566.png)
- 属性管理器-连接器-输入中配置附加依赖项，添加python310.lib，如果安装的是其他python版本，将lib名称改为对应的python库就好了

# 演示代码

```C++
#include "NxMonkey.h"

#undef slots
#include <Python.h>
#define slots Q_SLOTS

#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Py_Initialize();
    PyRun_SimpleString("print('Hello world')");
    PyObject* obj = Py_BuildValue("s", "F:\\Github\\Python-Games\\Pong\\game.py");
    FILE* file = _Py_fopen_obj(obj, "r+");
    if (file != NULL)
    {
        PyRun_SimpleFile(file, "game.py");
    }
    Py_Finalize();


    /*NxMonkey w;
    w.show();*/
    return a.exec();
}

```

代码中演示了通过加入python引用，然后调用Py_Initialize初始化运行环境，然后调用已经写好的python代码文件，运行完成后调用Py_Finalize(）清理运行环境。

# 问题处理

在调试过程中，出现了C2059, c2238 意外的标识符位于‘；’之前的问题，这个问题是由于QT中定义了slots作为关键字，而python3中有使用slots作为变量，所以引起冲突，解决办法有两种，一种是打开python安装目录下include的文件夹，找到object.h文件，按照下图添加两行代码以解决slots的冲突。

![img](v2-26c189a6230ca7183f52696bb309ed7e_720w.jpg)

还有一种办法是，在引用python.h文件的地方，直接undef slots，具体代码如下：

![image-20220612184619927](image-20220612184619927.png)

