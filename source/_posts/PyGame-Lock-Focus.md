---
title: PyGame锁定焦点
date: 2022-06-29 10:35:23
tags: Python
author: seven
description: 介绍如何锁定pygame焦点，防止失焦
---

# Pygame 锁定焦点

在pygame运行在windows平台下时，软件会和其他应用共享鼠标和键盘以及其他外设事件，导致有时pygame程序会失去焦点。如果想要保持pygame一直获取焦点，则需要执行下面的代码：

```python
pygame.event.set_grab(True)
```

上述代码会锁定输入事件到pygame上，需要注意的是，上述代码需要在执行pygame的显示初始化之后再进行设置，否则会抛出异常。比如按照下述初始化，是可以正常执行的：

```python
pygame.display.set_mode((1920,1080))
pygame.event.set_grab(True)
```

而如果按照下面的代码执行，则会抛出异常：

```python
pygame.event.set_grab(True)
pygame.display.set_mode((1920,1080))
```

